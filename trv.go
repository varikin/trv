package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

var pattern string

func usage() {
	fmt.Println("usage: trv FILENAME")
}

func substringWalker(path string, info os.FileInfo) {
	if strings.Contains(info.Name(), pattern) {
		fmt.Println(path)
	}
}

func main() {
	if len(os.Args) != 2 {
		usage()
	} else {

		pattern = os.Args[1]

		filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
			if err != nil {
				fmt.Printf("Error reading %s: %s\n", path, err.Error())
				return filepath.SkipDir
			}
			go substringWalker(path, info)
			return nil
		})
	}
}
